		LIST		p=16F84A
		__CONFIG	03FF1H
STATUS		equ		03h
PORTB		equ		06h
TRISB		equ		06h
timer		equ		10h
Reg_1		equ		0Ch
Reg_2		equ		0Dh

		org 0
		clrf		PORTB
		bsf		STATUS,5
		clrf		TRISB
		bsf		TRISB,7
		bcf		STATUS,5
		clrf		timer
start		btfss		PORTB,7 
		goto 		start 
		call		Nota_1_1000
		call 		Nota_2_2000
		call		Nota_1_1000
		goto		start

;--------------------- NOTA1
Nota_1_1000
		movlw		.1000
		movwf		timer

Nota_1_		decfsz		timer,1 
		goto 		Nota_1
		return			

Nota_1	bsf			PORTB,0
		call		delay_1 
		bcf		PORTB,0
		goto		Nota_1_

delay_1		movlw       	.8
        	movwf       	Reg_1
        	movlw       	.2
        	movwf       	Reg_2
        	decfsz      	Reg_1,F
        	goto        	$-1
        	decfsz      	Reg_2,F
        	goto        	$-3
        	nop
		return

;--------------------- NOTA2
Nota_2_2000
		movlw		.2000
		movwf		timer

Nota_2_		decfsz		timer,1 
		goto 		Nota_2 
		return			

Nota_2		bsf		PORTB,0
		call		delay_2 
		bcf		PORTB,0
		goto		Nota_2_ 

delay_2		movlw       	.199
        	movwf       	Reg_1
        	decfsz      	Reg_1,F
        	goto        	$-1
        	nop
        	nop
		return

		end